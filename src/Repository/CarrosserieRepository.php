<?php

namespace App\Repository;

use App\Entity\Carrosserie;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Carrosserie|null find($id, $lockMode = null, $lockVersion = null)
 * @method Carrosserie|null findOneBy(array $criteria, array $orderBy = null)
 * @method Carrosserie[]    findAll()
 * @method Carrosserie[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CarrosserieRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Carrosserie::class);
    }

    // /**
    //  * @return Carrosserie[] Returns an array of Carrosserie objects
    //  */

    /*
    public function findByVoiture($id)
    {
        return $this->createQueryBuilder('c')
            ->innerJoin('c.cle_voiture', 'v')
            ->addSelect('v')
            ->where('v.id = :val')
            ->setParameter('val', $id)
            ->getQuery()
            ->getOneOrNullResult();
    }
    */

    /*
    public function findOneBySomeField($value): ?Carrosserie
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}

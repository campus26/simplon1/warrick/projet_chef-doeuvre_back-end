<?php

namespace App\Controller;

use App\Entity\Voiture;
use App\Form\VoitureType;
use App\Repository\VoitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

/**
 * @Route("/voiture")
 */
class VoitureController extends AbstractController
{
    /**
     * @Route("/", name="voiture_index", methods={"GET"})
     */
    public function index(VoitureRepository $voitureRepository): Response
    {
        $voiture = $voitureRepository->findAll();
        return $this->json($voiture);
    }

    /**
     * @Route("/new", name="voiture_new", methods={"POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {

        $datas = $request->toArray();

        $matriculeReceived = $datas['matricule'];
        $carburanTypeReceived = $datas['carburan_type'];
        $cheveauxReceived = $datas['cheveaux'];
        $adresseReceived = $datas['adresse'];
        $prixReceived = $datas['prix'];

        $entityManager = $this->getDoctrine()->getManager();

        $voitureRepository = new Voiture();
        $voitureRepository->setMatricule($matriculeReceived);
        $voitureRepository->setCarburanType($carburanTypeReceived);
        $voitureRepository->setCheveaux($cheveauxReceived);
        $voitureRepository->setAdresse($adresseReceived);
        $voitureRepository->setPrix($prixReceived);

        $errors = $validator->validate($voitureRepository);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return new Response($errorsString);
        }
        
        $entityManager->persist($voitureRepository);
        $entityManager->flush();
        
        $voiture=$voitureRepository->getId();

        return new JsonResponse(
            [
                'id'    => $voiture,
                '$matriculeReceived' => $matriculeReceived,
                '$carburanTypeReceived' => $carburanTypeReceived,
                '$cheveauxReceived' => $cheveauxReceived,
                '$adresseReceived' => $adresseReceived,
                '$prixReceived' => $prixReceived,


            ]
        );
    }

    /**
     * @Route("/{id}", name="voiture_show", methods={"GET"})
     */
    public function show(Voiture $voiture): Response
    {
        return $this->json(['voiture' => $voiture]);
    }

    /**
     * @Route("/{id}/edit", name="voiture_edit", methods={"POST"})
     */
    public function edit(Request $request, Voiture $voiture, ValidatorInterface $validator): Response
    {

        $datas = $request->toArray();

        $matriculeReceived = $datas['matricule'];
        $carburanTypeReceived = $datas['carburan_type'];
        $cheveauxReceived = $datas['cheveaux'];
        $adresseReceived = $datas['adresse'];
        $prixReceived = $datas['prix'];

        $entityManager = $this->getDoctrine()->getManager();

        // $carrosserie = new Carrosserie();

        $voiture->setMatricule($matriculeReceived);
        $voiture->setCarburanType($carburanTypeReceived);
        $voiture->setCheveaux($cheveauxReceived);
        $voiture->setAdresse($adresseReceived);
        $voiture->setPrix($prixReceived);

        $errors = $validator->validate($voiture);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $entityManager->persist($voiture);
        $entityManager->flush();

        $voiture=$voiture->getId();

        return $this->json(
            [
                'status'    => 'Edit / Modif Voiture confirmé !',

                'id'    => $voiture,
                '$matriculeReceived' => $matriculeReceived,
                '$carburanTypeReceived' => $carburanTypeReceived,
                '$cheveauxReceived' => $cheveauxReceived,
                '$adresseReceived' => $adresseReceived,
                '$prixReceived' => $prixReceived,
            ]
        );
    }

    /**
     * @Route("/{id}/delete", name="voiture_delete", methods={"POST"})
     */
    public function delete(Request $request, Voiture $voiture): Response
    {
        if ($this->isCsrfTokenValid('delete'.$voiture->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($voiture);
            $entityManager->flush();
        }

        return new JsonResponse(
            [
                'status'    => 'Delete Voiture confirmé !',
            ]
        );
    }
}

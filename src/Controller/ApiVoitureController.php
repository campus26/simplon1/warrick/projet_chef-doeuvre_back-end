<?php

namespace App\Controller;

use App\Entity\Carrosserie;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Voiture;
use App\Repository\VoitureRepository;
use App\Repository\CarrosserieRepository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

   /**
     * @Route("/v1")
     */
class ApiVoitureController extends AbstractController
{
    public function __construct(CarrosserieRepository $carrosserieRepository)
    {
        $this->carrosserieRepository = $carrosserieRepository;
    }

    /**
     * @Route("/", name="api_get_voiture", methods={"GET"})
     */
    public function apiGetVoiture(VoitureRepository $voitureRepository): Response
    {
        // $user = $this->getUser();
        $voiture = $voitureRepository->findAll();
        return $this->json($voiture, 200, [], ['groups' => 'getVoiture']);
    }

    /**
     * 
     * @Route("/post", name="api_post_voiture", methods={"GET","POST"})
     */
    public function apiPostVoiture(Request $request, ValidatorInterface $validator): Response
    {
        $datas = $request->toArray();

        $matriculeReceived = $datas['matricule'];
        $carburanTypeReceived = $datas['carburan_type'];
        $cheveauxReceived = $datas['cheveaux'];
        $adresseReceived = $datas['adresse'];
        $prixReceived = $datas['prix'];

        $entityManager = $this->getDoctrine()->getManager();

        $voitureRepository = new Voiture();
        $voitureRepository->setMatricule($matriculeReceived);
        $voitureRepository->setCarburanType($carburanTypeReceived);
        $voitureRepository->setCheveaux($cheveauxReceived);
        $voitureRepository->setAdresse($adresseReceived);
        $voitureRepository->setPrix($prixReceived);

        $errors = $validator->validate($voitureRepository);
        if (count($errors) > 0) {
            $errorsString = (string) $errors;
            return new Response($errorsString);
        }
        
        $entityManager->persist($voitureRepository);
        $entityManager->flush();
        
        $voiture=$voitureRepository->getId();

        return new JsonResponse(
            [
                'id'    => $voiture,
                '$matriculeReceived' => $matriculeReceived,
                '$carburanTypeReceived' => $carburanTypeReceived,
                '$cheveauxReceived' => $cheveauxReceived,
                '$adresseReceived' => $adresseReceived,
                '$prixReceived' => $prixReceived,


            ]
        );
    }

    /**
     * @Route("/edit/{id}", name="voiture_edit", methods={"POST"})
     */
    public function edit(Request $request, Voiture $voitureRepository, ValidatorInterface $validator): Response
    {
        $datas = $request->toArray();

        $matriculeReceived = $datas['matricule'];
        $carburanTypeReceived = $datas['carburan_type'];
        $cheveauxReceived = $datas['cheveaux'];
        $adresseReceived = $datas['adresse'];
        $prixReceived = $datas['prix'];

        $entityManager = $this->getDoctrine()->getManager();

        // $carrosserie = new Carrosserie();

        $voitureRepository->setMatricule($matriculeReceived);
        $voitureRepository->setCarburanType($carburanTypeReceived);
        $voitureRepository->setCheveaux($cheveauxReceived);
        $voitureRepository->setAdresse($adresseReceived);
        $voitureRepository->setPrix($prixReceived);

        $errors = $validator->validate($voitureRepository);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $entityManager->persist($voitureRepository);
        $entityManager->flush();

        return $this->json(
            [
                'status'    => 'Edit / Modif Voiture confirmé !',
            ]
        );
    }
}

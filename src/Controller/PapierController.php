<?php

namespace App\Controller;

use App\Entity\Papier;
use App\Form\PapierType;
use App\Repository\PapierRepository;
use App\Repository\VoitureRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/papier")
 */
class PapierController extends AbstractController
{
    private $voitureRepository;
    
    public function __construct(VoitureRepository $voitureRepository) 
    {
        $this->voitureRepository = $voitureRepository;
    }

    /**
     * @Route("/", name="papier_index", methods={"GET"})
     */
    public function index(PapierRepository $papierRepository): Response
    {
        return $this->json($papierRepository->findAll());
    }

    /**
     * @Route("/new", name="papier_new", methods={"GET","POST"})
     */
    public function new(Request $request, ValidatorInterface $validator): Response
    {
        $datas = $request->toArray();

        $cleVoiture = $datas['cle_voiture'];
        $assuranceVoiture = $datas['assurance_voiture'];
        $carteGrise = $datas['carte_grise'];
        $controleTech = $datas['controle_tech'];
        $permisConduire = $datas['permis_conduire'];

        $entityManager = $this->getDoctrine()->getManager();

        $papier = new Papier();
        $papier->setCleVoiture(($this->voitureRepository->find($cleVoiture)));
        $papier->setAssuranceVoiture($assuranceVoiture);
        $papier->setCarteGrise($carteGrise);
        $papier->setControleTech($controleTech);
        $papier->setPermisConduire($permisConduire);

        $errors = $validator->validate($papier);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $entityManager->persist($papier);
        $entityManager->flush();

        return new JsonResponse(
            [
                'status'    => 'New Papier confirmé !',

                'cle_voiture'    => $cleVoiture,
                'assurance_voiture' => $assuranceVoiture,
                'carte_grise' => $carteGrise,
                'controle_tech' => $controleTech,
                'permis_conduire' => $permisConduire,
            ]
        );
    }

    /**
     * @Route("/{id}", name="papier_show", methods={"GET"})
     */
    public function show(Papier $papier): Response
    {
        return $this->json($papier);
    }

    /**
     * @Route("/{id}/edit", name="papier_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Papier $papier, ValidatorInterface $validator): Response
    {
        $datas = $request->toArray();

        $cleVoiture = $datas['cle_voiture'];
        $assuranceVoiture = $datas['assurance_voiture'];
        $carteGrise = $datas['carte_grise'];
        $controleTech = $datas['controle_tech'];
        $permisConduire = $datas['permis_conduire'];

        $entityManager = $this->getDoctrine()->getManager();

        $papier->setCleVoiture($cleVoiture);
        $papier->setAssuranceVoiture($assuranceVoiture);
        $papier->setCarteGrise($carteGrise);
        $papier->setControleTech($controleTech);
        $papier->setPermisConduire($permisConduire);

        $errors = $validator->validate($papier);
        if (count($errors) > 0) {

            $errorsString = (string) $errors;

            return new Response($errorsString);
        }

        $entityManager->persist($papier);
        $entityManager->flush();

        return $this->redirectToRoute('papier_index');


        return new JsonResponse(
            [
                'status'    => 'Edit / Modif Papier confirmé !',

                'cle_voiture'    => $cleVoiture,
                'assurance_voiture' => $assuranceVoiture,
                'carte_grise' => $carteGrise,
                'controle_tech' => $controleTech,
                'permis_conduire' => $permisConduire,
            ]
        );
    }

    /**
     * @Route("/{id}", name="papier_delete", methods={"POST"})
     */
    public function delete(Request $request, Papier $papier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $papier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($papier);
            $entityManager->flush();
        }

        return new JsonResponse(
            [
                'status'    => 'Delete Papier confirmé !',
            ]
        );
    }
}

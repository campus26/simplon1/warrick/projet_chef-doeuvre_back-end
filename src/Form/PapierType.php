<?php

namespace App\Form;

use App\Entity\Papier;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PapierType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('assurance_voiture')
            ->add('carte_grise')
            ->add('controle_tech')
            ->add('permis_conduire')
            ->add('cle_voiture')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Papier::class,
        ]);
    }
}

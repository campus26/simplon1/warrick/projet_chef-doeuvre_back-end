<?php

namespace App\Entity;

use App\Repository\InterieurRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=InterieurRepository::class)
 */
class Interieur
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siege_av_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siege_av_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siege_ar_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $siege_ar_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sol_conducteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sol_passager_av;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $sol_passager_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_port_av_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_port_av_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_port_ar_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_port_ar_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_capot;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_coffre;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_reservoir;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $volant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $levier_vitesse;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frein_main;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pedal_accelerateur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pedal_frein;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $pedal_embrayage;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $open_boite_gants;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tissu_porte_av_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tissu_porte_av_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tissu_porte_ar_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tissu_porte_ar_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tissu_plafond;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plage_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $radio;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $tableau_bord;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clignotant_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $clignotant_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $feux_crois;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $feux_route;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $feux_plein_fare;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $essui_glass;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $retro;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $retro_d;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $retro_g;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ventilation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $enceinte_av;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $enceinte_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ceinture_av;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $ceinture_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $propreter;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getSiegeAvD(): ?string
    {
        return $this->siege_av_d;
    }

    public function setSiegeAvD(string $siege_av_d): self
    {
        $this->siege_av_d = $siege_av_d;

        return $this;
    }

    public function getSiegeAvG(): ?string
    {
        return $this->siege_av_g;
    }

    public function setSiegeAvG(string $siege_av_g): self
    {
        $this->siege_av_g = $siege_av_g;

        return $this;
    }

    public function getSiegeArD(): ?string
    {
        return $this->siege_ar_d;
    }

    public function setSiegeArD(string $siege_ar_d): self
    {
        $this->siege_ar_d = $siege_ar_d;

        return $this;
    }

    public function getSiegeArG(): ?string
    {
        return $this->siege_ar_g;
    }

    public function setSiegeArG(string $siege_ar_g): self
    {
        $this->siege_ar_g = $siege_ar_g;

        return $this;
    }

    public function getSolConducteur(): ?string
    {
        return $this->sol_conducteur;
    }

    public function setSolConducteur(string $sol_conducteur): self
    {
        $this->sol_conducteur = $sol_conducteur;

        return $this;
    }

    public function getSolPassagerAv(): ?string
    {
        return $this->sol_passager_av;
    }

    public function setSolPassagerAv(string $sol_passager_av): self
    {
        $this->sol_passager_av = $sol_passager_av;

        return $this;
    }

    public function getSolPassagerAr(): ?string
    {
        return $this->sol_passager_ar;
    }

    public function setSolPassagerAr(string $sol_passager_ar): self
    {
        $this->sol_passager_ar = $sol_passager_ar;

        return $this;
    }

    public function getOpenPortAvD(): ?string
    {
        return $this->open_port_av_d;
    }

    public function setOpenPortAvD(string $open_port_av_d): self
    {
        $this->open_port_av_d = $open_port_av_d;

        return $this;
    }

    public function getOpenPortAvG(): ?string
    {
        return $this->open_port_av_g;
    }

    public function setOpenPortAvG(string $open_port_av_g): self
    {
        $this->open_port_av_g = $open_port_av_g;

        return $this;
    }

    public function getOpenPortArD(): ?string
    {
        return $this->open_port_ar_d;
    }

    public function setOpenPortArD(string $open_port_ar_d): self
    {
        $this->open_port_ar_d = $open_port_ar_d;

        return $this;
    }

    public function getOpenPortArG(): ?string
    {
        return $this->open_port_ar_g;
    }

    public function setOpenPortArG(string $open_port_ar_g): self
    {
        $this->open_port_ar_g = $open_port_ar_g;

        return $this;
    }

    public function getOpenCapot(): ?string
    {
        return $this->open_capot;
    }

    public function setOpenCapot(string $open_capot): self
    {
        $this->open_capot = $open_capot;

        return $this;
    }

    public function getOpenCoffre(): ?string
    {
        return $this->open_coffre;
    }

    public function setOpenCoffre(string $open_coffre): self
    {
        $this->open_coffre = $open_coffre;

        return $this;
    }

    public function getOpenReservoir(): ?string
    {
        return $this->open_reservoir;
    }

    public function setOpenReservoir(string $open_reservoir): self
    {
        $this->open_reservoir = $open_reservoir;

        return $this;
    }

    public function getVolant(): ?string
    {
        return $this->volant;
    }

    public function setVolant(string $volant): self
    {
        $this->volant = $volant;

        return $this;
    }

    public function getLevierVitesse(): ?string
    {
        return $this->levier_vitesse;
    }

    public function setLevierVitesse(string $levier_vitesse): self
    {
        $this->levier_vitesse = $levier_vitesse;

        return $this;
    }

    public function getFreinMain(): ?string
    {
        return $this->frein_main;
    }

    public function setFreinMain(string $frein_main): self
    {
        $this->frein_main = $frein_main;

        return $this;
    }

    public function getPedalAccelerateur(): ?string
    {
        return $this->pedal_accelerateur;
    }

    public function setPedalAccelerateur(string $pedal_accelerateur): self
    {
        $this->pedal_accelerateur = $pedal_accelerateur;

        return $this;
    }

    public function getPedalFrein(): ?string
    {
        return $this->pedal_frein;
    }

    public function setPedalFrein(string $pedal_frein): self
    {
        $this->pedal_frein = $pedal_frein;

        return $this;
    }

    public function getPedalEmbrayage(): ?string
    {
        return $this->pedal_embrayage;
    }

    public function setPedalEmbrayage(string $pedal_embrayage): self
    {
        $this->pedal_embrayage = $pedal_embrayage;

        return $this;
    }

    public function getOpenBoiteGants(): ?string
    {
        return $this->open_boite_gants;
    }

    public function setOpenBoiteGants(string $open_boite_gants): self
    {
        $this->open_boite_gants = $open_boite_gants;

        return $this;
    }

    public function getTissuPorteAvD(): ?string
    {
        return $this->tissu_porte_av_d;
    }

    public function setTissuPorteAvD(string $tissu_porte_av_d): self
    {
        $this->tissu_porte_av_d = $tissu_porte_av_d;

        return $this;
    }

    public function getTissuPorteAvG(): ?string
    {
        return $this->tissu_porte_av_g;
    }

    public function setTissuPorteAvG(string $tissu_porte_av_g): self
    {
        $this->tissu_porte_av_g = $tissu_porte_av_g;

        return $this;
    }

    public function getTissuPorteArD(): ?string
    {
        return $this->tissu_porte_ar_d;
    }

    public function setTissuPorteArD(string $tissu_porte_ar_d): self
    {
        $this->tissu_porte_ar_d = $tissu_porte_ar_d;

        return $this;
    }

    public function getTissuPorteArG(): ?string
    {
        return $this->tissu_porte_ar_g;
    }

    public function setTissuPorteArG(string $tissu_porte_ar_g): self
    {
        $this->tissu_porte_ar_g = $tissu_porte_ar_g;

        return $this;
    }

    public function getTissuPlafond(): ?string
    {
        return $this->tissu_plafond;
    }

    public function setTissuPlafond(string $tissu_plafond): self
    {
        $this->tissu_plafond = $tissu_plafond;

        return $this;
    }

    public function getPlageAr(): ?string
    {
        return $this->plage_ar;
    }

    public function setPlageAr(string $plage_ar): self
    {
        $this->plage_ar = $plage_ar;

        return $this;
    }

    public function getRadio(): ?string
    {
        return $this->radio;
    }

    public function setRadio(string $radio): self
    {
        $this->radio = $radio;

        return $this;
    }

    public function getTableauBord(): ?string
    {
        return $this->tableau_bord;
    }

    public function setTableauBord(string $tableau_bord): self
    {
        $this->tableau_bord = $tableau_bord;

        return $this;
    }

    public function getClignotantD(): ?string
    {
        return $this->clignotant_d;
    }

    public function setClignotantD(string $clignotant_d): self
    {
        $this->clignotant_d = $clignotant_d;

        return $this;
    }

    public function getClignotantG(): ?string
    {
        return $this->clignotant_g;
    }

    public function setClignotantG(string $clignotant_g): self
    {
        $this->clignotant_g = $clignotant_g;

        return $this;
    }

    public function getFeuxCrois(): ?string
    {
        return $this->feux_crois;
    }

    public function setFeuxCrois(string $feux_crois): self
    {
        $this->feux_crois = $feux_crois;

        return $this;
    }

    public function getFeuxRoute(): ?string
    {
        return $this->feux_route;
    }

    public function setFeuxRoute(string $feux_route): self
    {
        $this->feux_route = $feux_route;

        return $this;
    }

    public function getFeuxPleinFare(): ?string
    {
        return $this->feux_plein_fare;
    }

    public function setFeuxPleinFare(string $feux_plein_fare): self
    {
        $this->feux_plein_fare = $feux_plein_fare;

        return $this;
    }

    public function getEssuiGlass(): ?string
    {
        return $this->essui_glass;
    }

    public function setEssuiGlass(string $essui_glass): self
    {
        $this->essui_glass = $essui_glass;

        return $this;
    }

    public function getRetro(): ?string
    {
        return $this->retro;
    }

    public function setRetro(string $retro): self
    {
        $this->retro = $retro;

        return $this;
    }

    public function getRetroD(): ?string
    {
        return $this->retro_d;
    }

    public function setRetroD(string $retro_d): self
    {
        $this->retro_d = $retro_d;

        return $this;
    }

    public function getRetroG(): ?string
    {
        return $this->retro_g;
    }

    public function setRetroG(string $retro_g): self
    {
        $this->retro_g = $retro_g;

        return $this;
    }

    public function getVentilation(): ?string
    {
        return $this->ventilation;
    }

    public function setVentilation(string $ventilation): self
    {
        $this->ventilation = $ventilation;

        return $this;
    }

    public function getEnceinteAv(): ?string
    {
        return $this->enceinte_av;
    }

    public function setEnceinteAv(string $enceinte_av): self
    {
        $this->enceinte_av = $enceinte_av;

        return $this;
    }

    public function getEnceinteAr(): ?string
    {
        return $this->enceinte_ar;
    }

    public function setEnceinteAr(?string $enceinte_ar): self
    {
        $this->enceinte_ar = $enceinte_ar;

        return $this;
    }

    public function getCeintureAv(): ?string
    {
        return $this->ceinture_av;
    }

    public function setCeintureAv(string $ceinture_av): self
    {
        $this->ceinture_av = $ceinture_av;

        return $this;
    }

    public function getCeintureAr(): ?string
    {
        return $this->ceinture_ar;
    }

    public function setCeintureAr(string $ceinture_ar): self
    {
        $this->ceinture_ar = $ceinture_ar;

        return $this;
    }

    public function getPropreter(): ?string
    {
        return $this->propreter;
    }

    public function setPropreter(string $propreter): self
    {
        $this->propreter = $propreter;

        return $this;
    }
}

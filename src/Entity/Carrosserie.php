<?php

namespace App\Entity;

use App\Repository\CarrosserieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CarrosserieRepository::class)
 */
class Carrosserie
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="text")
     */
    private $facad_av;

    /**
     * @ORM\Column(type="text")
     */
    private $facad_ar;

    /**
     * @ORM\Column(type="text")
     */
    private $facad_left;

    /**
     * @ORM\Column(type="text")
     */
    private $facad_right;

    /**
     * @ORM\Column(type="text")
     */
    private $toit;

    /**
     * @ORM\OneToOne(targetEntity=Voiture::class, cascade={"persist", "remove"})
     */
    private $cle_voiture;

    /**
     * @ORM\OneToMany(targetEntity=Images::class, mappedBy="carrosserie", orphanRemoval=true)
     */
    private $images;

   



    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    
    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFacadAv(): ?string
    {
        return $this->facad_av;
    }

    public function setFacadAv(string $facad_av): self
    {
        $this->facad_av = $facad_av;

        return $this;
    }

    public function getFacadAr(): ?string
    {
        return $this->facad_ar;
    }

    public function setFacadAr(string $facad_ar): self
    {
        $this->facad_ar = $facad_ar;

        return $this;
    }

    public function getFacadLeft(): ?string
    {
        return $this->facad_left;
    }

    public function setFacadLeft(string $facad_left): self
    {
        $this->facad_left = $facad_left;

        return $this;
    }

    public function getFacadRight(): ?string
    {
        return $this->facad_right;
    }

    public function setFacadRight(string $facad_right): self
    {
        $this->facad_right = $facad_right;

        return $this;
    }

    public function getToit(): ?string
    {
        return $this->toit;
    }

    public function setToit(string $toit): self
    {
        $this->toit = $toit;

        return $this;
    }

    public function getCleVoiture(): Voiture
    {
        return $this->cle_voiture;
    }

    public function setCleVoiture(Voiture $cle_voiture): self
    {
        $this->cle_voiture = $cle_voiture;

        return $this;
    }

    /**
     * @return Collection|Images[]
     */
    public function getImages(): Collection
    {
        return $this->images;
    }

    public function addImage(Images $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images[] = $image;
            $image->setCarrosserie($this);
        }

        return $this;
    }

    public function removeImage(Images $image): self
    {
        if ($this->images->removeElement($image)) {
            // set the owning side to null (unless already changed)
            if ($image->getCarrosserie() === $this) {
                $image->setCarrosserie(null);
            }
        }

        return $this;
    }
}

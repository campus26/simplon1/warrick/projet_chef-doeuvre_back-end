<?php

namespace App\Entity;

use App\Repository\VoitureRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ORM\Entity(repositoryClass=VoitureRepository::class)
 */
class Voiture
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     * @Groups("getVoiture")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("getVoiture")
     */
    private $matricule;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("getVoiture")
     */
    private $carburan_type;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("getVoiture")
     */
    private $cheveaux;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("getVoiture")
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups("getVoiture")
     */
    private $prix;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMatricule(): ?string
    {
        return $this->matricule;
    }

    public function setMatricule(string $matricule): self
    {
        $this->matricule = $matricule;

        return $this;
    }

    public function getCarburanType(): ?string
    {
        return $this->carburan_type;
    }

    public function setCarburanType(string $carburan_type): self
    {
        $this->carburan_type = $carburan_type;

        return $this;
    }

    public function getCheveaux(): ?string
    {
        return $this->cheveaux;
    }

    public function setCheveaux(string $cheveaux): self
    {
        $this->cheveaux = $cheveaux;

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = $adresse;

        return $this;
    }

    public function getPrix(): ?string
    {
        return $this->prix;
    }

    public function setPrix(string $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

}

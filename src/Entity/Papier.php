<?php

namespace App\Entity;

use App\Repository\PapierRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PapierRepository::class)
 */
class Papier
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $assurance_voiture;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $carte_grise;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $controle_tech;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $permis_conduire;

    /**
     * @ORM\OneToOne(targetEntity=Voiture::class, cascade={"persist", "remove"})
     */
    private $cle_voiture;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAssuranceVoiture(): ?string
    {
        return $this->assurance_voiture;
    }

    public function setAssuranceVoiture(string $assurance_voiture): self
    {
        $this->assurance_voiture = $assurance_voiture;

        return $this;
    }

    public function getCarteGrise(): ?string
    {
        return $this->carte_grise;
    }

    public function setCarteGrise(string $carte_grise): self
    {
        $this->carte_grise = $carte_grise;

        return $this;
    }

    public function getControleTech(): ?string
    {
        return $this->controle_tech;
    }

    public function setControleTech(string $controle_tech): self
    {
        $this->controle_tech = $controle_tech;

        return $this;
    }

    public function getPermisConduire(): ?string
    {
        return $this->permis_conduire;
    }

    public function setPermisConduire(string $permis_conduire): self
    {
        $this->permis_conduire = $permis_conduire;

        return $this;
    }

    public function getCleVoiture(): Voiture
    {
        return $this->cle_voiture;
    }

    public function setCleVoiture(Voiture $cle_voiture): self
    {
        $this->cle_voiture = $cle_voiture;

        return $this;
    }
}

<?php

namespace App\Entity;

use App\Repository\MecaniqueRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=MecaniqueRepository::class)
 */
class Mecanique
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $frein;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $suspension;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $triangle;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $cardant;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $essieu_ar;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $disc_frein;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $moteur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $radiateur;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $batterie;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $courroie_distri;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFrein(): ?string
    {
        return $this->frein;
    }

    public function setFrein(string $frein): self
    {
        $this->frein = $frein;

        return $this;
    }

    public function getSuspension(): ?string
    {
        return $this->suspension;
    }

    public function setSuspension(string $suspension): self
    {
        $this->suspension = $suspension;

        return $this;
    }

    public function getTriangle(): ?string
    {
        return $this->triangle;
    }

    public function setTriangle(string $triangle): self
    {
        $this->triangle = $triangle;

        return $this;
    }

    public function getCardant(): ?string
    {
        return $this->cardant;
    }

    public function setCardant(string $cardant): self
    {
        $this->cardant = $cardant;

        return $this;
    }

    public function getEssieuAr(): ?string
    {
        return $this->essieu_ar;
    }

    public function setEssieuAr(string $essieu_ar): self
    {
        $this->essieu_ar = $essieu_ar;

        return $this;
    }

    public function getDiscFrein(): ?string
    {
        return $this->disc_frein;
    }

    public function setDiscFrein(string $disc_frein): self
    {
        $this->disc_frein = $disc_frein;

        return $this;
    }

    public function getMoteur(): ?string
    {
        return $this->moteur;
    }

    public function setMoteur(string $moteur): self
    {
        $this->moteur = $moteur;

        return $this;
    }

    public function getRadiateur(): ?string
    {
        return $this->radiateur;
    }

    public function setRadiateur(string $radiateur): self
    {
        $this->radiateur = $radiateur;

        return $this;
    }

    public function getBatterie(): ?string
    {
        return $this->batterie;
    }

    public function setBatterie(string $batterie): self
    {
        $this->batterie = $batterie;

        return $this;
    }

    public function getCourroieDistri(): ?string
    {
        return $this->courroie_distri;
    }

    public function setCourroieDistri(string $courroie_distri): self
    {
        $this->courroie_distri = $courroie_distri;

        return $this;
    }
}
